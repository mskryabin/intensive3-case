def add_years_name(age):
    if 11 <= age % 100 <= 19:
        years_name = 'лет'
    elif age % 10 == 1:
        years_name = 'год'
    elif 2 <= age % 10 <= 4:
        years_name = 'года'
    else:
        years_name = 'лет'
    return f'{age} {years_name}'
